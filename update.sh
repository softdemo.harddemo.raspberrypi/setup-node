#!/bin/bash

BASE_NAME=setup-node
THIS=update.sh
TOOL_DIR=/usr/local/etc/$BASE_NAME
PACKAGE_DIR_NAME=cluster-packages
PACKAGE_CACHE_DIR=/var/opt/$PACKAGE_DIR_NAME
TEMP_DIR=$PACKAGE_CACHE_DIR/temp
GIT_URL="https://gitlab.com/softdemo.harddemo.raspberrypi/$PACKAGE_DIR_NAME"

# import common utility
. $TOOL_DIR/util/*.sh

setup_env_profile() {
# setup .env-profile
env_profile=./.env-profile
if [ ! -f $env_profile ]; then
  touch $env_profile
  chkhost=`hostname`
  if [ $chkhost = "controller" -o $chkhost = "p0" ]; then
    echo "export ENV_PROFILE_TESTENV=no" >> $env_profile
  else
    echo "export ENV_PROFILE_TESTENV=yes" >> $env_profile
  fi
fi
. $env_profile
}

root_check

setup_env_profile

step_echo "Installing git..."
apt-get -y install git

# clean up temp dir
step_echo "Cleaning up temp directory..."
if [ ! -d $TEMP_DIR ]; then
  mkdir -p $TEMP_DIR
fi
workdir=$TEMP_DIR/$PACKAGE_DIR_NAME
rm -fr $workdir > /dev/null 2>&1

# git clone cluster packages
cd $TEMP_DIR
step_echo "Downloading package information..."
step_echo "Git clone from $GIT_URL..."
git clone $GIT_URL
if [ $? -ne 0 ]; then
  error_exit "Failed to download package information."
else
  step_echo "Information successfully downloaded."
fi

# finally update list and packages
cp -r $workdir/master.list $workdir/agent.list $PACKAGE_CACHE_DIR
ct=0
chkfile=/tmp/update.sh.chk.$$
cat /dev/null > $chkfile 
for line in `cat $workdir/master.list $workdir/agent.list`
do
  mode=`echo $line | sed "s/^\(.\).*/\1/"`
  package=`echo $line | sed "s/^.\([^_]*\)_.*$/\1/"`
  version=`echo $line | sed "s/^.[^_]*_\(.*\)$/\1/"`
  curpkg="${package}_$version"
  grep $curpkg $chkfile
  if [ $? -ne 0 ]; then
    echo $curpkg >> $chkfile
    pkgdir=$PACKAGE_CACHE_DIR/packages/$package/$version
    srcdir=$workdir/packages/$package/$version
    pkggz=$package.tgz
    if [ ! -f $pkgdir/$pkggz -o "x$mode" = "x!" ]; then
      # check if build.sh script exists
      if [ -x $srcdir/build.sh ]; then
        step_echo "Building package${EDCOL} ${STCOL_YEL}$package ($version)$EDCOL..."
        cd $srcdir; ./build.sh
        if [ $? -ne 0 ]; then
          error_exit "Failed to build package."
        fi
        # compress build image if exist
        if [ -d $srcdir/$package ]; then
          echo "Compressing built image..."
          cd $srcdir; tar zcf $pkggz ./$package
          if [ $? -ne 0 ]; then
            error_exit "Failed to compress package."
          else 
            rm -fr ./$package > /dev/null 2>&1 
          fi
        fi
      fi
      step_echo "Updating$EDCOL $STCOL_SYN$curpkg$EDCOL."
      # copy
      destdir=$PACKAGE_CACHE_DIR/packages/$package/$version 
      mkdir -p $destdir
      cp -r $srcdir/* $destdir/
      prevpkg=$curpkg
      ct=`expr $ct + 1`
    else 
      step_echo "Package ${EDCOL}${STCOL_YEL}$package($version)$EDCOL ${STCOL_GRN}already exists in local cache, skipped."
    fi
    prevpkg=$curpkg
  fi
done
rm -fr $chkfile > /dev/null 2>&1
# remove tempdir 
rm -fr $TEMP_DIR/* > /dev/null 2>&1

if [ $ct -eq 0 ]; then
  normal_exit "No package to update."
else
  normal_exit "$ct package(s) successfully updated."
fi


