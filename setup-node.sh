#!/bin/bash

#
# Cluster Node setup tool
#  version 1.0
#
#  usage: setup-node.sh master|agent <cluster number|node number>
#       executed by root user
#

# common setting
BASE_NAME=setup-node
THIS=$BASE_NAME.sh
TOOL_HOME=/usr/local/etc
TOOL_DIR=$TOOL_HOME/$BASE_NAME
PACKAGE_DIR_NAME=cluster-packages
PACKAGE_CACHE_DIR=/var/opt/$PACKAGE_DIR_NAME
env_profile=./.env-profile

# import common utility
. $TOOL_DIR/util/*.sh

. $env_profile

# check if user is root
root_check

# check argument
if [ $# -ne 2 ]; then
  error_exit "Usage $THIS master|agent <cluster-number|node-number>"
  else 
    if [ $1 != "master" -a $1 != "agent" ]; then
      error_exit "Argument $1 is invalid. 'master' or 'agent' is valid."
    fi 
fi

# check cluster or node number
if [ $1 == "master" ]; then
  # cluster number >= 1
  errmsg="Invalid argument '$2' for master cluster number. It must be integer and >0 value."
  expr $2 + 1 > /dev/null 2>&1
  if [ $? -gt 1 ]; then
    error_exit "$errmsg"
  else
    retval=`expr $2 + 1`
    if [ $retval -lt 2 ]; then
      error_exit "$errmsg"
    fi
  fi
else
  # check node number
  errmsg="Invalid argument '$2' for agent node number. It must be 0 to 4."
  expr $2 + 1 > /dev/null 2>&1
  if [ $? -gt 1 ]; then
    error_exit "$errmsg"
  else
    if [ $2 -gt 4 -o $2 -lt 0 ]; then
      error_exit "$errmsg"
    fi
  fi
fi

target_host() {
nodenum=$1

# detect if environment is test or real
shorthostname=`hostname | sed "s/^\([^\.]*\).*$/\1/"`
if [ $shorthostname = "p0" -o "x$SIMULATE" != "x" ]; then
  # real environment
  echo "p$nodenum"
else
  # test environment
  nodenum=`expr $nodenum + 1`
  echo "ubuntu$nodenum"
fi
}

SETUP_TYPE=$1
HOSTNAMETEMPL="p<Node Number>.n<Cluster Number>.picluster"
TARGET_LIST=$PACKAGE_CACHE_DIR/$SETUP_TYPE.list
SCRIPTS_DIR=$PACKAGE_CACHE_DIR/packages
INSTALLER=install.sh
UNINSTALLER=uninstall.sh
CLUSTER_NUMBER_FILE=$TOOL_DIR/.cluster.num
REMOTE=no
if [ $SETUP_TYPE = "agent" ]; then
  NODE_NUMBER=$2
  TARGET_HOST=`target_host $NODE_NUMBER`
  if [ $NODE_NUMBER -ne 0 ]; then
    REMOTE=yes
    SSH_USER=pi
    # can change ssh user by setting environment variable 'ALT_USER'
    if [ "x$ALT_USER" != "x" ]; then
      SSH_USER=$ALT_USER
    fi
  fi
  REPOSITORY=$TOOL_DIR/.repository.agent.$TARGET_HOST
else
  NODE_NUMBER=0
  REPOSITORY=$TOOL_DIR/.repository.master
  # add Node Number def to env-profile
  sed -i "/^export ENV_PROFILE_NODE_NUMBER/d" $env_profile > /dev/null 2>&1
  echo "export ENV_PROFILE_NODE_NUMBER=${NODE_NUMBER}" >> $env_profile
fi

# copy public key for ssh to remote host only 1 time
if [ $REMOTE = yes ]; then
  # check if local public key exists
  ssh_pub_key=/root/.ssh/id_rsa.pub
  if [ ! -f $ssh_pub_key ]; then
    # create SSH public key
    msg_out INFO "Creating SSH public key..."
    ssh-keygen -t rsa -P '' -f /root/.ssh/id_rsa
    if [ $? -ne 0 ]; then
      if [ $SSH_USER = "pi" ]; then
        echo -e "If you change from ${STCOL_GRN}pi${EDCOL} user to other, set ${STCOL_YEL}ALT_USER${EDCOL} environment variable."
      fi
      error_exit "Unable to create SSH public key."
    else
      msg_out INFO "Created SSH public key."
    fi
  fi
  remote_host=`target_host $NODE_NUMBER`
  authorized_keys=$TOOL_DIR/.authorized_keys.$remote_host 
  if [ ! -f $authorized_keys ]; then
    # create authorized_keys file and copy to remote host
    touch $authorized_keys
    chmod 600 $authorized_keys
    cat $ssh_pub_key >> $authorized_keys
    msg_out WARN "*** Must be entered SSH password ***"
    scp $authorized_keys $SSH_USER@$remote_host:.ssh/authorized_keys
  fi
fi


warn_exit() {
  add_warn_count
  msg=$1
  echo -e "${STCOL_YEL}*WARN*:${EDCOL} $msg"
  last_msg "Canceled"
  exit 0
}

normal_exit() {
  msg=$1
  echo -e "${STCOL_BLU}*OK*:${EDCOL} $msg"
  last_msg "Finished"
  exit 0
}

check_scripts() {
  _mode=$1
  _package=$2
  _version=$3
  scriptsdir=$SCRIPTS_DIR/$_package/$version
  installer=no
  uninstaller=no
  if [ ! -d $scriptsdir ]; then
    echo -n "Package directory for $_package ($version) not found."
    return
  fi
  if [ -x $scriptsdir/$INSTALLER ]; then
    installer=yes
  fi
  if [ -x $scriptsdir/$UNINSTALLER ]; then
    uninstaller=yes
  fi
  installed=no
  # check repository
  if [ -f $REPOSITORY ]; then
    # check record
    grep "^${_package}_" $REPOSITORY > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      installed=yes
      preversion=`grep "^${_package}_" $REPOSITORY |    \
                sed "s/^${_package}_//" |       \
                sed "s/:.*//"`
    fi
  fi
  commonpart="for $_package ($_version) not found."
  case $_mode in
    "+")
        preuninstaller=$SCRIPTS_DIR/$_package/$preversion/uninstall.sh
	if [ $installed = yes -a $preuninstaller = no ]; then
          echo -n "Executable uninstaller for $STCOL_CYN$_package$EDCOL ($preversion) not found."
          return 
        fi
        if [ $installer = no ]; then
          echo -n "Executable installer  $STCOL_CYN$_package$EDCOL ($_version) not found."
	  return
        fi
	;;
    "-")
        if [ $uninstaller = no ]; then
          echo -n "Executable uninstaller $commonpart"
          return
        fi
	;;
    "!")
	if [ $uninstaller = no ]; then
	  echo -n "Executable uninstaller $commonpart"
	  return
        else
          if [ $installer = no ]; then
            echo -n "Executable installer $commonpart"
	    return
          fi
        fi
	;;
  esac
  echo -n "OK"
}

add_repository() {
  __package=$1
  __version=$2

  timestr=`date +"%Y%m%d-%H%M%S"`
  grep "^${__package}_" $REPOSITORY > /dev/null 2>&1
  if [ $? -eq 0 ]; then
    # delete information
    tmpfile=/tmp/$THIS.$$
    sed "/^${__package}_/d" $REPOSITORY > $tmpfile
    mv -f $tmpfile $REPOSITORY
  fi
  # add new information
  echo "${__package}_${__version}:$timestr" >> $REPOSITORY
}

remove_repository() {
  __package=$1
  __version=$2

  # delete information
  tmpfile=/tmp/$THIS.$$
  sed "/^${__package}_/d" $REPOSITORY > $tmpfile
  mv -f $tmpfile $REPOSITORY
}

exec_remote() {
__package=$1
__version=$2
__scriptdir=$3
__script=$4

msg_out INFO "Copy package $STCOL_SYN$__package$EDCOL ($__version) to $STCOL_GRN$TARGET_HOST$EDCOL."

if [ "x$ENV_PROFILE_TESTENV" = "xyes" ]; then
  if [ "x$ALT_USER" = "x" ]; then
    msg_out WARN "May not exist user '$SSH_USER' in TEST environment, define ALT_USER environment variable to set SSH user account."
  fi
fi

# create script directory if not exist
ssh $SSH_USER@$TARGET_HOST "sudo mkdir -p $SCRIPTS_DIR; mkdir -p /tmp/packages/$__package"
if [ $? -ne 0 ]; then
  error_exit "Unable to copy package '$__package'"
fi
# copy package script to remote host
pkgdir=$SCRIPTS_DIR/$__package
ssh $SSH_USER@$TARGET_HOST sudo mkdir -p $pkgdir > /dev/null 2>&1
if [ $? -ne 0 ]; then
  error_exit "Unable to copy package '$__package'"
fi
ssh $SSH_USER@$TARGET_HOST sudo chown -R $SSH_USER $pkgdir
if [ $? -ne 0 ]; then
  error_exit "Unable to copy package '$__package'"
fi
pkg=$SCRIPTS_DIR/$__package/$__version
rsync -r $pkg $SSH_USER@$TARGET_HOST:$pkgdir 
if [ $? -ne 0 ]; then
  error_exit "Failed to copy package to $TARGET_HOST."
fi
ssh $SSH_USER@$TARGET_HOST sudo chown -R root.root $pkgdir
if [ $? -ne 0 ]; then
  error_exit "Unable to copy package '$__package'"
fi

# uncompress if package has tgz file
hastgz=no
tgz=$pkg/$__package.tgz
if [ -f $tgz ]; then
  hastgz=yes
  msg_out INFO "Uncompress built image..."
  ssh $SSH_USER@$TARGET_HOST "cd $__scriptdir && sudo tar zxf $__package.tgz"
  if [ $? -ne 0 ]; then
    error_exit "Unable to uncompress built images."
  fi
fi

# execute script
msg_out INFO "Execute $__script remotely on host $STCOL_GRN$TARGET_HOST$EDCOL."
ssh $SSH_USER@$TARGET_HOST "cd $__scriptdir && sudo ./$__script $cluster_number $node_number"

# remove temporaly unzip image
if [ "x$hastgz" = "xyes" ]; then
  msg_out INFO "Remove temporal unzip image..."
  ssh $SSH_USER@$TARGET_HOST "cd $__scriptdir && sudo rm -fr $__package"
  if [ $? -ne 0 ]; then
    msg_out WARN "Failed to remove temporal image file '$tgz'"
  fi
fi

}

exec_scripts() {
  _mode=$1
  _package=$2
  _version=$3
  scriptsdir=$SCRIPTS_DIR/$_package/$_version
  installer=install.sh
  uninstaller=uninstall.sh

  installed=no
  preversion=""
  # check repository
  if [ -f $REPOSITORY ]; then
    # check record
    grep "^${_package}_" $REPOSITORY > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      installed=yes
      preversion=`grep "^${_package}_" $REPOSITORY |	\
		sed "s/^${_package}_//" | 	\
		sed "s/:.*//"`
    fi
  fi
  # temporally unzip built image 
  hastgz=no
  tgz=$scriptsdir/$_package.tgz
  if [ $REMOTE != yes -a -f $tgz ]; then
    hastgz=yes
    msg_out INFO "Uncompress built image..."
    cd $scriptsdir; tar zxf $_package.tgz
    if [ $? -ne 0 ]; then
      error_exit "Failed to uncompress built image '$tgz'"
    fi
  fi
  case $_mode in
    [+!])
	if [ $installed = yes ]; then
	  if [ "x$_mode" = "x+" -a "x$_version" = "x$preversion" ]; then
	    msg_out INFO "$STCOL_SYN$_package$EDCOL ($version) already installed, ${STCOL_GRN}skipped$EDCOL."
            return
	  else
	   if [ "x$preversion" != "x" -o "x$mode" = "x!" ]; then
             if [ "x$preversion" != "x$_version" ]; then
	       msg_out WARN "Another version of $STCOL_SYN$_package$EDCOL already exists, updating $STCOL_GRN$preversion$EDCOL to $STCOL_GRN$_version$EDCOL"
	     else
	       msg_out INFO "Re-installing $STCOL_SYN$_package$EDCOL"
	     fi
	     # uninstll pre version
             preuninstaller=uninstall.sh
	     preuninstallerdir=$SCRIPTS_DIR/$_package/$preversion
	     if [ ! -x $preuninstallerdir/$preuninstaller ]; then
               msg_out WARN "Uninstaller for $STCOL_SYN$_package$EDCOL ($preversion) not found, skipped to uninstall."
             else
                # execute uninstaller
                msg_out INFO "Uninstall $STCOL_SYN$_package$EDCOL ($preversion)..."
	        if [ $REMOTE != yes ]; then
		  # exec locally
                  cd $preuninstallerdir; ./$preuninstaller $cluster_number $node_number
		else
		  # exec remotely
		  exec_remote $_package $_version $preuninstallerdir $preuninstaller
		fi
                if [ $? -ne 0 ]; then
                  msg_out WARN "Failed to uninstall $STCOL_SYN$_package$EDCOL ($preversion). Check log message above."
                else
	          msg_out INFO "Unistall $STCOL_SYN$_package$EDCOL ($preversion) successfully finished."
                fi
             fi
	     remove_repository $_package $preversion
	   fi 
	  fi
	fi
        # exec installer
        if [ $REMOTE != yes ]; then
          # exec locally
          cd $scriptsdir; ./$installer $cluster_number $node_number
        else
          # exec remotely
          exec_remote $_package $_version $scriptsdir $installer
        fi
        if [ $? -ne 0 ]; then
          msg_out ERROR "Failed to install  $STCOL_SYN$_package$EDCOL ($_version). Check log message above."
          error_exit "Stop to setup."
        else
          msg_out INFO "$STCOL_SYN$_package$EDCOL ($_version) successfully installed."
        fi
        add_repository $_package $_version
	;;
    "-")
        if [ $installed = yes ]; then
	  # uninstall
          if [  $REMOTE != yes ]; then
            # exec locally
	    cd $scriptsdir; ./$uninstaller $cluster_number $node_number
          else
            # exec remotely
            exec_remote $_package $_version $scriptsdir $uninstaller
          fi
	  if [ $? -ne 0 ]; then
            msg_out ERROR "Failed to uninstall  $STCOL_SYN$_package$EDCOL ($_version). Check log message above."
            error_exit "Stop to setup."
          else
            msg_out INFO "$STCOL_SYN$_package$EDCOL ($_version) successfully removed."
	    remove_repository $_package $_version
          fi
        else
	  msg_out INFO "$STCOL_SYN$_package$EDCOL ($_version) not installed, skipped."
        fi
	;;
  esac

  if [ "x$hastgz" = "xyes" ]; then
     cd $scriptsdir; rm -fr $_package 
     if [ $? -ne 0 ]; then
       msg_out WARN "Failed to remove temporal unzip image."
     fi
  fi
}

loop_exec() {
  exec_mode=$1
  ct=1
  for line in `grep -e '^[+!-]' $TARGET_LIST`
  do
    mode=`echo $line | cut -c 1`
    wk1=`echo $line | cut -c 2-`
#    wk2=`echo $wk1 | sed "s/\:.*//g"`
    package=`echo $wk1 | sed "s/_.*$//"`
    version=`echo $wk1 | sed "s/${package}_//"`
    pkgver=`echo $version | sed "s/_.*$//"`
    revision=`echo $version | sed "s/${pkgver}_//"`
#    wk3=`echo $line | sed "s/^[^:]*://"`
#    user=`echo $wk3 | sed "s/:.*//"`
#    group=`echo $wk3 | sed "s/$user://"`
    case $exec_mode in    
	CHECK)
	  msg_out -n INFO "Checking $STCOL_SYN$package$EDCOL ($version)..."
          ret=`check_scripts $mode $package $version`
          if [[ $ret != "OK" ]]; then
            echo "NG"
	    msg_out ERROR "$ret"
            error_exit "Must be run update.sh at first, or setup scripts are not valid."
          else
            echo "OK"
          fi
	  ;;
	EXEC)
	  msg_out -s $ct INFO "Executing $STCOL_SYN$package$EDCOL ($version)..."
          exec_scripts $mode $package $version
	  ;;
	*)
	  ;;
    esac
    ct=`expr $ct + 1`
  done
  if [ $exec_mode == "EXEC" ]; then
    msg_out -s $ct INFO "All packages are processed."
  fi
}

# start message
upper_setup_type=`echo $SETUP_TYPE | sed "s/^\(.*\)$/\U\1/"`
msg_out INFO "Now starting to setup $STCOL_GRN$upper_setup_type$EDCOL node."

# check environment
if [ ! -d $TOOL_DIR ]; then
  msg_out ERROR "Tool directory '$TOOL_DIR' not found."
  error_exit "Check if tool is installed correctly."
else
  msg_out INFO "Tool directory '$TOOL_DIR' found."
fi
if [ ! -f $TARGET_LIST ]; then
  msg_out ERROR "Target list file '$TARGET_LIST' not found."
  error_exit "Check if tool is installed correctly."
else
  msg_out INFO "Target file '$TARGET_LIST' found."
fi
steps_num=`grep -e '^[+!-]' $TARGET_LIST | wc -l`
if [ $steps_num -eq 0 ]; then
  warn_exit "No setup information in the target list."
else
  msg_out INFO "The number of installation steps is $steps_num."
fi
if [ ! -d $SCRIPTS_DIR ]; then
  msg_out ERROR "Scripts directory '$SCRIPTS_DIR' not found."
  error_exit "Check if tool is installed correctly."
fi 

cluster_number=""
node_number=""

# get hostname
if [ $SETUP_TYPE == "master" ]; then
  cluster_number=$2
  node_number="0"
  # make cluster number file
  echo -n $cluster_number > $CLUSTER_NUMBER_FILE
else
  if [ ! -f $CLUSTER_NUMBER_FILE ]; then
    error_exit "Cluster number file not found. Master setup is needed at first."
  fi
  cluster_number=`cat $CLUSTER_NUMBER_FILE`
  node_number=$2
fi
hostname=`echo -n $HOSTNAMETEMPL |    \
          sed "s/<Cluster Number>/$cluster_number/g" |    \
          sed "s/<Node Number>/$node_number/"`

msg_out INFO "Setup started for node '$STCOL_GRN$hostname$EDCOL'."

# check each installation script
msg_out INFO "Checking all setup scripts existence."
loop_exec CHECK
msg_out INFO "All setup scripts exist, continue to setup."

# execute each script
loop_exec EXEC


# display repository information
msg_out INFO "Showing current repository information."
echo "---------- Repository updated -----------"
echo "Type:$SETUP_TYPE"
cat $REPOSITORY
echo "-----------------------------------------"

# finish
normal_exit "Setup successfully finished."
