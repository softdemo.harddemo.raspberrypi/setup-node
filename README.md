setup-node  Cluster Node Setup tool
================

## ツールインストール
    cd /usr/local/etc  
    sudo git clone https://gitlab.com/softdemo.harddemo.raspberrypi/setup-node
  
## パッケージリストの更新（インターネット）
インストール直後やパッケージが更新された際にツール実行前に必ず行うこと。  
aptツールで言うところのapt-get updateのような機能。  
  
`sudo /usr/local/etc/setup-node/update.sh`

## パッケージリストの更新（ローカルネット）
１つのMasterノードでupdate.shを実行後は他のMasterノードはupdate.shではなくupdate-local.shを実行することでローカルコピーが行えます。  
これによりインターネットの回線スピードを気にすることなくパッケージリスト更新が複数ノードに対して行えます。また更新後のビルド処理も省略されるため高速です。    
`sudo /usr/local/etc/setup-node/update-local.sh <hostname|ip address>`  
_hostnameまたはip addressにはパッケージリスト更新済みのMasterノードのものを指定して下さい。_
  
## ツール実行
■Masterセットアップ  

`sudu -E /usr/local/etc/setup-node/setup-node.sh master <cluster-number>`  

_**cluster-number**_ : クラスター番号(p?.nX.piclusterのXに相当)  
  
■Agentセットアップ  

`sudo -E /usr/local/etc/setup-node/setup-node.sh agent <node-number>`

_**node-number**_ : 0から4を指定。(pY.n?.piclusterのYに相当)   
※各Agentノードの初回セットアップ時にSSHの公開鍵認証設定のためにSSHパスワードを入力する必要あり





