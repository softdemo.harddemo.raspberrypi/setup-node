#!/bin/bash
# update repository from local master node which is already updated
# usage :
#    update-local.sh <hostname|ip address>

BASE_NAME=setup-node
THIS=update.sh
TOOL_DIR=/usr/local/etc/$BASE_NAME
PACKAGE_DIR_NAME=cluster-packages
PACKAGE_CACHE_DIR=/var/opt/$PACKAGE_DIR_NAME
TEMP_DIR=$PACKAGE_CACHE_DIR/temp
SSH_USER="pi"

# import common utility
. $TOOL_DIR/util/*.sh

setup_env_profile() {
# setup .env-profile
env_profile=./.env-profile
if [ ! -f $env_profile ]; then
  touch $env_profile
  chkhost=`hostname`
  if [ $chkhost = "controller" -o $chkhost = "p0" ]; then
    echo "export ENV_PROFILE_TESTENV=no" >> $env_profile
  else
    echo "export ENV_PROFILE_TESTENV=yes" >> $env_profile
  fi
fi
. $env_profile
}

# root check
root_check

# argument check
if [ $# -ne 1 ]; then
  error_exit "Usage : update-local.sh <hostname|ip address>"
else
  TARGET_HOST=$1
fi

rsync_repository() {
  if [ "x$ALT_USER" != "x" ]; then
    SSH_USER=$ALT_USER
  else 
    if [ "x$ENV_PROFILE_TESTENV" = "xyes" ]; then
      step_echo "May not exist user '${STCOL_YEL}$SSH_USER$STCOL_GRN' in TEST environment, define ALT_USER environment variable to set SSH user account."
    fi
  fi
  srcdir=$PACKAGE_CACHE_DIR
  rsyncdest=/var/opt
  cd $rsyncdest; rsync -avhr --exclude='.*' $SSH_USER@$TARGET_HOST:$srcdir .
  if [ $? -ne 0 ]; then
    error_exit "Unable to copy repository from $TARGET_HOST."
  fi
}

setup_env_profile
rsync_repository

normal_exit "Repository successfully cloned."

