#!/bin/bash

#
# common utility
#

# color setting
EDCOL="\033[0m"
STCOL_RED="\033[0;31m"
STCOL_BLU="\033[0;34m"
STCOL_YEL="\033[0;33m"
STCOL_GRN="\033[0;32m"
STCOL_SYN="\033[0;36m"

root_check() {
# check if user is root
if [ `whoami` != "root" ]; then
  error_exit "Execute this tool as ${STCOL_RED}root${EDCOL} user."
fi
}

warn_count=0
error_count=0

msgcounter=1
msg_out() {
  step=""
  if [ $1 = "-n" ]; then
    level=$2
    msg=$3
    nowrapopt="-n"
  else
    if [ $1 = "-s" ]; then
      step=$2
      level=$3
      msg=$4
    else
      level=$1
      msg=$2
    fi
    nowrapopt=""
  fi
  case $level in
    "INFO")
        levelcolor=$STCOL_BLU
        ;;
    "WARN")
        levelcolor=$STCOL_YEL
        add_warn_count
        ;;
    "ERROR")
        levelcolor=$STCOL_RED
        add_error_count
        ;;
    *)
        levelcolor=""
        ;;
  esac
  linenum=`printf "%04d" $msgcounter`
  levelf=`printf "%-5s" $level`
  timestr=`date +"%Y-%m-%d %H:%M:%S"`
  if [ "x$step" != "x" ]; then
    allsteps=`expr $steps_num + 1`
    tmpval=`expr $step \* 100`
    tmpval=`expr $tmpval / $allsteps`
    stepstr="($tmpval%)"
  else
    stepstr=""
  fi
#  echo $nowrapopt -e "$linenum $timestr ${levelcolor}[${levelf}]${EDCOL} $msg"
  echo $nowrapopt -e "$timestr ${levelcolor}[${levelf}]${EDCOL} $stepstr $msg"

  msgcounter=`expr $msgcounter + 1`
}

add_warn_count() {
  warn_count=`expr $warn_count + 1`
}
add_error_count() {
  error_count=`expr $error_count + 1`

}

last_msg() {
  msg=$1
  if [ $warn_count -gt 0 ]; then
                                         warnctmsg="$STCOL_YEL$warn_count WARN$EDCOL"
  else
    warnctmsg="$warn_count WARN"
  fi
  if [ $error_count -gt 0 ]; then
    errorctmsg="$STCOL_RED$error_count ERROR$EDCOL"
  else
    errorctmsg="$error_count ERROR"
  fi
  echo -e "$msg [ $warnctmsg, $errorctmsg ]."
}

error_exit() {
  add_error_count
  msg=$1
  echo -e "${STCOL_RED}*ERROR*:${EDCOL} $msg"
  last_msg "Aborted"
  exit 1
}

warn_exit() {
  add_warn_count
  msg=$1
  echo -e "${STCOL_YEL}*WARN*:${EDCOL} $msg"
  last_msg "Canceled"
  exit 0
}

normal_exit() {
  msg=$1
  echo -e "${STCOL_BLU}*OK*:${EDCOL} $msg"
  last_msg "Finished"
  exit 0
}

step_echo() {
msg=$1
echo -e "${STCOL_GRN}$msg${EDCOL}"
}

