#!/bin/bash

BASE_NAME=setup-node
THIS=cleanup-cache.sh
TOOL_DIR=/usr/local/etc/$BASE_NAME
TEMP_DIR=/var/opt
PACKAGE_DIR_NAME=cluster-packages
CACHE_DIR=/var/opt/$PACKAGE_DIR_NAME/packages

. $TOOL_DIR/util/*.sh

env_profile=.env-profile

. $env_profile

step_echo() {
msg=$1
echo -e "${STCOL_GRN}$msg${EDCOL}"
}

root_check

# prompt
while [ 1 ] 
do
  echo -n -e "${STCOL_GRN}Is it ok to REMOVE ALL local package cashe ?$EDCOL [y/N] : "
  read answer
  if [ "x$answer" = "x" ]; then
    answer=N
  fi
  case $answer in
    [yY])
        answer=Y
        break
	;;
    [nN])
	answer=N
	warn_exit "Canceled."
	break
	;;
    *)
	echo "The answer must be 'y', 'n' or only ENTER for 'n'."
	;;
  esac
done

step_echo "Removing all cluster package cache of this host."
rm -fr $CACHE_DIR/* 
if [ $? -ne 0 ]; then
  error_exit "Failed to clean up cache completly, check message avobe."
fi
step_echo "Package chache has been cleaned up."

normal_exit "To re-create cashe, execute 'update.sh'."

